# 3 ruby compare strings test

Tópicos avançados 2:

- Verificando diferença entre strings
- https://dojopuzzles.com/problems/verificando-diferenca-entre-strings/

- **OUTPUT DojoPuzzles:** "O pássaro [amarel]o caiu. O pássaro [vermelh]o caiu."
- **OUTPUT Meu\_\_\_\_\_\_\_\_\_:** "O pássaro [amar]el[o] caiu. O pássaro [verm]el[ho] caiu."

## First time requirements (Devs):

```sh
gpg --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby
source ~/.rvm/scripts/rvm
\curl -sSL https://get.rvm.io | bash -s stable --rails
ruby -v # Check RUBY version
rvm -v  # Check RVM version
rvm requirements

# Using the latest version
#/bin/bash --login
rvm install 3.0.2
rvm use 3.0.2 --default
```

## Ruby project Requirements

```sh
# Install within the project directory
gem install bundler --no-document
gem install rspec
gem update

bundle install
bundle update

# Initialize RSPEC
rspec --init
```

## Running manually the tests

```sh
rspec
```

## Where are the Ruby files?

**The common ruby files are inside [src](src/) folder.**

## Where are the TEST files?

**Tests needs to be under the [spec](spec/) folder.**

## More info

- https://semaphoreci.com/community/tutorials/getting-started-with-rspec
