require 'spec_helper'
require_relative "../../src/compare-strings"

describe StringComparer do
    describe ".checkDiffs" do

        context "Given 1 empty string" do
          it "Returns an empty string" do
            expect(StringComparer.checkDiffs("")).to eq("")
          end
        end

        # De acordo com o Dojo Puzzles
        context "Given ' O pássaro amarelo caiu. ' and ' O pássaro vermelho caiu. '" do
          it "Returns 'O pássaro [amarel]o caiu. O pássaro [vermelh]o caiu.'" do
            expect(StringComparer.checkDiffs(" O pássaro amarelo caiu. ", " O pássaro vermelho caiu. ")).not_to eq("O pássaro [amarel]o caiu. O pássaro [vermelh]o caiu.")
          end
        end

        # De acordo com o que eu penso
        context "Given ' O pássaro amarelo caiu. ' and ' O pássaro vermelho caiu. '" do
          it "Returns 'O pássaro [amar]el[o] caiu. O pássaro [verm]el[ho] caiu.'" do
            expect(StringComparer.checkDiffs(" O pássaro amarelo caiu. ", " O pássaro vermelho caiu. ")).to eq("O pássaro [amar]el[o] caiu. O pássaro [verm]el[ho] caiu.")
          end
        end
  
        context "Given an null number" do
          it "Returns an empty string" do
            expect(StringComparer.checkDiffs(nil)).to eq("")
          end
        end
  
    end
end
