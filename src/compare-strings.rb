class StringComparer
    def self.checkDiffs(s1 = "", s2 = "")
        # Output the small differences between each word on the phrase
        arr1 = s1.to_s.strip().split(" ")
        arr2 = s2.to_s.strip().split(" ")
        puts "Arr1 = #{arr1} and \nArr2 = #{arr2}"

        if arr1.size >= arr2.size
            maxArrSize = arr1.size
        else
            maxArrSize = arr2.size
        end

        res1 = ""
        res2 = ""
        for iWord in (0..maxArrSize-1) do
            aword1 = arr1[iWord].split("")
            aword2 = arr2[iWord].split("")
            # puts "[#{iWord}] - Word Arr1: #{aword1}"
            # puts "[#{iWord}] - Word Arr2: #{aword2}"

            if aword1.size >= aword2.size
                maxStrSize = aword1.size
            else
                maxStrSize = aword2.size
            end
            
            isBracketOn = false
            for iChar in (0..maxStrSize-1) do
                if (aword1[iChar].nil? == false) and (aword2[iChar].nil? == false)

                    if aword1[iChar] == aword2[iChar]
                        #puts "EQUAL: #{aword1[iChar]} #{aword2[iChar]}"
                        if isBracketOn == true
                            res1 << "]"
                            res2 << "]"
                        end
                        isBracketOn = false
                        
                        res1 << aword1[iChar]
                        res2 << aword2[iChar]

                    elsif aword1[iChar] != aword2[iChar]
                        #puts "DIFF:  #{aword1[iChar]} #{aword2[iChar]}"
                        if isBracketOn == false
                            res1 << "["
                            res2 << "["
                        end
                        isBracketOn = true
                        
                        res1 << aword1[iChar]
                        res2 << aword2[iChar]
                        
                    end

                elsif (aword1[iChar].nil? == false)
                    res1 << aword1[iChar]
                elsif (aword2[iChar].nil? == false)
                    res2 << aword2[iChar]
                end

                # puts "#{res1} #{res2}"
            end
            if isBracketOn == true
                res1 << "] "
                res2 << "] "
            elsif isBracketOn == false
                res1 << " "
                res2 << " "

            end

        end

        result = res1.lstrip() + res2.strip()
        puts "The result '#{result}'"
        return result
    end
    
end

def main()
    c1 = StringComparer.new()
    #c1.checkDiffs("", "")
    #c1.checkDiffs("Teste", "Testa")
    #c1.checkDiffs(" O pássaro amarelo caiu. ", " O pássaro vermelho caiu. ")
end

#main()